# Visualization Smart Building Node-Red Dashboard

**Visualization of sensor data in tables and graphs**. The goal is to exploit to the maximum the data collected by the different sensors of the smart building


## Installation
### Prerequisites

To install Node-RED locally you will need a [supported version of Node.js](https://nodered.org/docs/faq/node-versions).

### Installing with npm

Then you can use this command to install node-red
```
sudo npm install -g --unsafe-perm node-red
```

Go to the directory created by node-red
Install the dashboard module :
```
npm install node-red-dashboard
```

Install the table module :
```
npm install node-red-node-ui-table
```

## Running

Once installed as a global module you can use the `node-red` command to start Node-RED in your terminal. You can use `Ctrl-C` or close the terminal window to stop Node-RED.

You can then access the Node-RED editor by pointing your browser at [http://localhost:1880](http://localhost:1880/).

## Import the project

On the Node-RED editor, you can then import the project by clicking on the **top left menu** and then **import**. Then you have to import the following json file:[Smart_Building_visualization](smart_building_visualization_flows.json)

> Instead of importing a file, you can also copy all the text from the json flow file and paste it in the same section **import**

## Deploy the project

Once imported, you can deploy the project at the top left by clicking on **Deploy**. To see the result you need to go there : [http://localhost:1880/ui](http://localhost:1880/ui).

> If you make a modification on Node-RED editor, you will have to deploy again to see the modifications on the dashboard

## License
This work is available under the [CeCILL-C](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) license.
